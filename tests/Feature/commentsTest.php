<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class taskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */

    /// tots poden comentar, pero el superusuari es el unic que pot eliminar.
    public function can_superadmin_list_comments()
    {
//       $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('GET', '/api/v1/comments', [],['Authorization' => $user->api_key]);
        $response->assertSuccessful();

    }

    /**
     * @test
     */
    public function can_regular_user_list_comments()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('GET', '/api/v1/comments', [],['Authorization' => $user->api_key]);
//        $response->assertStatus(403);
        $response->assertSuccessful();
    }
    /**
     * @test
     */
    public function can_superadmin_store_comments()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('POST', '/api/v1/comments', [
            'title' => 'Malament!',
            'content' => 'Este anime es mol fotut',
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }
    /**
     * @test
     */
    public function can_regular_user_store_comments()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('POST', '/api/v1/comments', [
            'title' => 'Malament!',
            'content' => 'Este anime es mol fotut',
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
//        $response->assertStatus(403);
        $response->assertSuccessful();

    }

    /**
     * @test
     */
    public function can_superuser_delete_comments()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('DELETE', '/api/v1/comments/{id}', [],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function can_regular_user_delete_comments()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('DELETE', '/api/v1/comments/{id}', [],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(403);

    }

    /**
     * @test
     */
    public function can_superuser_update_task()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.

        $response = $this->json('PUT', '/api/v1/comments/{id}', [
            'title' => 'BE!!',
            'content' => 'Este anime es mol bo!',
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function can_regular_user_update_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.

        $response = $this->json('PUT', '/api/v1/comments/{id}', [
            'title' => 'BE!!',
            'content' => 'Este anime es mol bo!',
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
//        $response->assertStatus(403);
        $response->assertSuccessful();
    }


}
