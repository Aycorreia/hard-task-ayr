<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class personatjesTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    // CRUD de personatjes


//    public function testExample()
//    {
//        $response = $this->get('/');
//
//        $response->assertStatus(200);
//    }

    /**
     * @test
     */

    public function regular_user_can_list_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => false
        ]);

        //fer peticions a la api
        $response = $this->json('GET', '/api/v1/personatjes',[],['Authorization' => $user->api_key]);
//        dump($response);
        $response->assertSuccessful();
    }

    /**
     * @test
     */

    public function superuser_can_list_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => true
        ]);

        //fer peticions a la api
        $response = $this->json('GET', '/api/v1/personatjes',[],['Authorization' => $user->api_key]);
        $response->assertSuccessful();
    }

    /**
     * @test
     */

    public function regular_user_can_not_create_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => false
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('POST', '/api/v1/personatjes',[
            'nom' => 'Martha',
            'cognom' => 'Forester Bright',
            'es_protag' => true
        ],['Authorization' => $user->api_key]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */

    public function superuser_can_create_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => true
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('POST', '/api/v1/personatjes',[
            'nom' => 'Martha',
            'cognom' => 'Forester Bright',
            'es_protag' => true
        ],['Authorization' => $user->api_key]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */

    public function regular_user_can_not_update_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => false
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('PUT', '/api/v1/personatjes/{id}',[
            'nom' => 'Martha',
            'cognom' => 'Forester Bright',
            'es_protag' => true
        ],['Authorization' => $user->api_key]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */

    public function superuser_can_update_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => true
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('PUT', '/api/v1/personatjes/{id}',[
            'nom' => 'Martha',
            'cognom' => 'Forester Bright',
            'es_protag' => true
        ],['Authorization' => $user->api_key]);
        $response->assertStatus(200);
    }

    /**
     * @test
     */

    public function regular_user_can_not_delete_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => false
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('DELETE', '/api/v1/personatjes/{id}',[],['Authorization' => $user->api_key]);
        $response->assertStatus(403);
    }

    /**
     * @test
     */

    public function superuser_can_delete_personatjes()
    {

//        $this->withoutExceptionHandling();
        //crea el usuari amb lo factory
        //ficar que el superadmin es false.
        $user=factory(User::class)->create([
            'superadmin' => true
        ]);

//        dump($user);

        //fer peticions a la api
        $response = $this->json('DELETE', '/api/v1/personatjes/{id}',[],['Authorization' => $user->api_key]);
        $response->assertStatus(200);
    }




}
