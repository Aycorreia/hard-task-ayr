<?php

namespace Tests\Feature;

use App\User;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class taskTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @test
     * @return void
     */
    public function can_superadmin_list_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('GET', '/api/v1/tasks', [],['Authorization' => $user->api_key]);
        $response->assertSuccessful();

    }

    /**
     * @test
     */
    public function can_regular_user_list_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('GET', '/api/v1/tasks', [],['Authorization' => $user->api_key]);
        $response->assertStatus(403);

    }
    /**
     * @test
     */
    public function can_superadmin_store_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('POST', '/api/v1/tasks', [
            'title' => 'Fer Menjar',
            'description' => 'Fer el sopar',
            'inici' => '2000-30-10',
            'fet' => true,
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }
    /**
     * @test
     */
    public function can_regular_user_store_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('POST', '/api/v1/tasks', [
            'title' => 'Fer Menjar',
            'description' => 'Fer el sopar',
            'inici' => '2000-30-10',
            'fet' => true,
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(403);

    }

    /**
     * @test
     */
    public function can_superuser_delete_task()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('DELETE', '/api/v1/tasks/{id}', [],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function can_regular_user_delete_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.
        $response = $this->json('DELETE', '/api/v1/tasks/{id}', [],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(403);

    }

    /**
     * @test
     */
    public function can_superuser_update_task()
    {
        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>true]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.

        $response = $this->json('PUT', '/api/v1/tasks/{id}', [
            'title' => 'rara',
            'description' => 'Fer el rere',
            'inici' => '2020-30-10',
            'fet' => false,
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(200);

    }

    /**
     * @test
     */
    public function can_regular_user_update_task()
    {
//        $this->withoutExceptionHandling();
        $user = factory(User::class)->create(['superadmin'=>false]);
        // GET ES READ, DELETE ES DELETE, POST ES CREAR, PUT ES UPDATE.

        $response = $this->json('PUT', '/api/v1/tasks/{id}', [
            'title' => 'rara',
            'description' => 'Fer el rere',
            'inici' => '2020-30-10',
            'fet' => false,
            'user_id' => '1' ],
            ['Authorization' => $user->api_key]);
        $response->assertStatus(403);

    }


}
