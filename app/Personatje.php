<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Personatje extends Model
{
    protected $fillable = [
        'user_id',
        'nom',
        'cognom',
        'es_protag'
    ];
}
