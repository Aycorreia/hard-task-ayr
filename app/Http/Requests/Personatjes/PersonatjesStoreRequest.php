<?php

namespace App\Http\Requests\Personatjes;

use App\User;
use Illuminate\Foundation\Http\FormRequest;

class PersonatjesStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        $user = User::where('api_key', $this->header('Authorization'))->firstOrFail();
        if (!is_null($user) && $user->superadmin) return true;
        else return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nom' => 'required|string',
            'cognom' => 'required|string',
            'es_protag' => 'required|boolean'
        ];
    }
}
