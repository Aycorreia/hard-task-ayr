<?php

namespace App\Http\Controllers;

use App\Comment;
use App\Http\Requests\Comments\CommentDestroyRequest;
use App\Http\Requests\Comments\CommentIndexRequest;
use App\Http\Requests\Comments\CommentStoreRequest;
use App\Http\Requests\Comments\CommentUpdateRequest;
use App\User;
use Illuminate\Http\Request;

class CommentsController extends Controller
{
    public function index(CommentIndexRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Comment::all();
    }
    public function store(CommentStoreRequest $request){
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        $comments = Comment::all();

    }
    public function destroy(CommentDestroyRequest $request){
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        $task = Comment::all();

    }
    public function update(CommentUpdateRequest $request){
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        $task = Comment::all();

    }
}
