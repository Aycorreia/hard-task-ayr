<?php

namespace App\Http\Controllers;

use App\Http\Requests\Personatjes\PersonatjesDestroyRequest;
use App\Http\Requests\Personatjes\PersonatjesIndexRequest;
use App\Http\Requests\Personatjes\PersonatjesStoreRequest;
use App\Http\Requests\Personatjes\PersonatjesUpdateRequest;
use App\Personatje;
use App\User;
use Illuminate\Http\Request;

class PersonatjesController extends Controller
{
    public static function index(PersonatjesIndexRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Personatje::all();
    }

    public static function store(PersonatjesStoreRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Personatje::all();
    }
    public static function update(PersonatjesUpdateRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Personatje::all();
    }
    public static function destroy(PersonatjesDestroyRequest $request)
    {
        User::where('api_key', $request->header('Authorization'))->firstOrFail();
        return Personatje::all();
    }

}
